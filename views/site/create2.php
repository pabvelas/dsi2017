<?php
use yii\helpers\html;
use yii\widgets\ActiveForm;
?>

<h1>Crear Pais </h>
<h3><?= $msg ?></h3>
<?php $form =ActiveForm::begin([
    "method" => "post",
    'enableClientValidation' => true,
    ]);

?>
<div class = "form-group">
<?= $form->field($model, "nombre")->input("text") ?>
</div>

<div class = "form-group">
<?= $form->field($model, "ciudad")->input("text") ?>
</div>

<div class = "form-group">
<?= $form->field($model, "comuna")->input("text") ?>
</div>

<?= Html::submitButton("Crear",["class" => "btn btn-primary"]) ?>

<?php $form->end() ?>